//
//  CustomView.swift
//  Demo_Calculator
//
//  Created by Thang on 8/31/20.
//  Copyright © 2020 Thang. All rights reserved.
//

import UIKit
import Foundation

enum TypeInput {
    case Number
    case Result
    case Equal
    
}

protocol CustomViewDelegate {
    func hamanhinh()
    func chuyenKQ(so: [String],tien: String)
    
}

class CustomView: UIView {
    
    @IBOutlet weak var lblHienThi: UILabel!
    @IBOutlet weak var lblBaoLoi: UILabel!
    
    var delegate:CustomViewDelegate!
    
    var contentView : UIView?
    
    var kieudanh: trangthaidanh!
    var typeInput: TypeInput = .Number
    var totalString: String = ""
    var xulysoString: String = ""
    var xulytienString: String = ""
    var mangSo:[String] = [String]()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override func prepareForInterfaceBuilder() {
        super.prepareForInterfaceBuilder()
        setup()
        contentView?.prepareForInterfaceBuilder()
    }
    
    func setup() {
        guard let view = loadViewFromNib() else { return }
        view.frame = bounds
        view.autoresizingMask =
            [.flexibleWidth, .flexibleHeight]
        addSubview(view)
        contentView = view
    }
    
    func loadViewFromNib() -> UIView? {
        let bundle = Bundle(for: type(of: self))
        let nib = UINib(nibName: "CustomView", bundle: bundle)
        return nib.instantiate(
            withOwner: self,
            options: nil).first as? UIView
    }

    @IBAction func didSelectedButton(_ sender: Any) {
        delegate.hamanhinh()
    }
    
    @IBAction func btnNumber(_ sender: SuaNut) {
        lblBaoLoi.text = ""
        if (typeInput == .Number) { // xu ly so
            // nếu là 3 càng thì gọi hàm xử lý số 3
            if kieudanh == .bacang {
                let kq = self.xulyso3(text: "\(sender.tag)")
                lblHienThi.text = kq

            }else {// nếu là xiên,đề,lô thì gọi hàm xử lý số 2
                let kq = self.xulyso2(text: "\(sender.tag)")
                 lblHienThi.text = kq
            }

        }else if (typeInput == .Equal) { // xu ly tien
            // goi ham xy ly tien
            let kq = self.xulytien(text: "\(sender.tag)")
            lblHienThi.text = lblHienThi.text! + "\(sender.tag)" 
        }

    }
    
    func xulytien(text: String) -> String {
        self.totalString.append(text)
        self.xulytienString.append(contentsOf: text)
        var kq = ""
        for kitu in xulytienString {
            kq.append(kitu)
        }
        return kq
    }
    
    func xulyso2(text:String) -> (String) {
        self.totalString.append(text)
        self.xulysoString.append(contentsOf: text)
        var kq = ""
        var count = 0
        // 2342353464
        for kitu in xulysoString {
            kq.append(kitu)
            if (count%2==1) {
                kq.append(",")
            }
            count += 1
        }
        return kq
    }
    func xulyso3(text: String) -> String {
        self.totalString.append(text)
        self.xulysoString.append(contentsOf: text)
        var kq = ""
        var count = 0
        for kitu in xulysoString {
            kq.append(kitu)
            if count % 3 == 2 {
                kq.append(",")
            }
            count += 1
        }
        return kq
    }
    
    @IBAction func btnXoa(_ sender: SuaNut) {
        reset()
    }
    
    @IBAction func btnBang(_ sender: SuaNut) {
        self.typeInput = .Equal
        lblHienThi.text?.removeLast()
        totalString.append("=")
        lblHienThi.text = lblHienThi.text! + "="
    }
    
    @IBAction func btnPhay(_ sender: Any) {
        
    }
    
    @IBAction func btnEnter(_ sender: Any) {
        
        if kieudanh == .xien {
            if xulysoString.count < 4 {
                lblBaoLoi.text = "chưa nhập đủ cặp số"
                reset()
                return
            }
        }
        if kieudanh == .bacang {
            if xulysoString.count < 3 {
                lblBaoLoi.text = "chưa nhập đủ số"
                reset()
                return
            }
        }
        if xulysoString.count <= 1 {
            // in ra chua co so
            lblBaoLoi.text = "chưa nhập số đánh"
            reset()
            return
        }
        if xulytienString.count < 1 || Int(xulytienString)! < 1 {
            // in ra chua nhap tien
            lblBaoLoi.text = "chưa nhập số tiền"
            reset()
            return
        }
        
        var so = ""
        var count = 0
        if kieudanh == .bacang {
            for item in xulysoString {
                so.append(item)
                if count % 3 == 2 {
                    mangSo.append(so)
                    so = ""
                }
                count += 1
            }

        }else {
            for item in xulysoString {
                so.append(item)
                if (count%2==1) {
                    mangSo.append(so)
                    so = ""
                }
                count += 1
            }
        }

//        print("so:" + "\(mangSo)")
//        print("tien:" + xulytienString)
        delegate.chuyenKQ(so: mangSo, tien: xulytienString)
        reset()
    }
    
    func reset(){
        typeInput = .Number
        xulysoString = ""
        xulytienString = ""
        totalString = ""
        lblHienThi.text = ""
        mangSo.removeAll()
    }
}
//    func xulyso(text:String) {
//        soLoDe += text
//        if soLoDe.count == 2 {
//            // luu lai
//            mangSo.append(soLoDe)
//            // xoa soloDe
//            soLoDe = ""
//
//            var kq = ""
//            for item in mangSo {
//                let str = item + ","
//                kq.append(contentsOf: str)
//            }
//            lblHienThi.text = kq
//        }else if soLoDe.count == 1 {
//            if mangSo.count == 0 {
//                lblHienThi.text = soLoDe
//            }else {
//                var kq = ""
//                for item in mangSo {
//                    let str = item + ","
//                    kq.append(contentsOf: str)
//                }
//                lblHienThi.text = kq + soLoDe
//            }
//        }
//
//    }
