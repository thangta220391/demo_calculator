//
//  DanhViewController.swift
//  Demo_Calculator
//
//  Created by Thang on 8/29/20.
//  Copyright © 2020 Thang. All rights reserved.
//

import UIKit
import Foundation

enum trangthaidanh {
    case chuaco
    case de
    case lo
    case bacang
    case xien
}
protocol DanhDelegate: class {
    func chuyenkq(de: [So], lo: [So], xien: [So], bacang: [So])
}

class DanhViewController: UIViewController,CustomViewDelegate {
    
    @IBOutlet weak var hidenTextField: UITextField!
    @IBOutlet weak var deLabel: UILabel!
    @IBOutlet weak var loLabel: UILabel!
    @IBOutlet weak var xienLabel: UILabel!
    @IBOutlet weak var baCangLabel: UILabel!
    
    var delegate: DanhDelegate?
    
    var customView:CustomView!
    
    var danhdau:trangthaidanh = .chuaco // 0 la de , 1 la lo, 2 la xien, 3 la ba cang
    var mangDe:[So] = []
    var mangLo:[So] = []
    var mangXien:[So] = []
    var mangBaCang:[So] = []
//    var mangquanly:[So] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
//        deLabel.numberOfLines = 0
//        deLabel.lineBreakMode = .byWordWrapping
        // custom ban phim
//        let view = UIView(frame: CGRect(x: 0, y: 0, width: 320, height: 250))
//        view.backgroundColor = .red
        // thêm sự kiện khi 1 chữ trong textfield thay đổi
//        hidenTextField.addTarget(self, action: #selector(textFieldDidChange), for: .editingChanged)
        // gán bàn phím của textfield bằng custom bàn phím
//        let view = UIView(frame: CGRect(x: 0, y: 0, width: 414, height: 400))
        customView = CustomView(frame: CGRect(x: 0, y: 0, width: 200, height: 460))
        customView.delegate = self
        customView.contentView?.backgroundColor = .black
        hidenTextField.inputView = customView
        // Do any additional setup after loading the view.
        
//        hienThi()
        // để label có nhiều dòng
        deLabel.numberOfLines = 0
        deLabel.lineBreakMode = .byWordWrapping
        loLabel.numberOfLines = 0
        loLabel.lineBreakMode = .byWordWrapping
        xienLabel.numberOfLines = 0
        xienLabel.lineBreakMode = .byWordWrapping
        baCangLabel.numberOfLines = 0
        baCangLabel.lineBreakMode = .byWordWrapping
    }
    
    func hienThiDe(mang:[String],tien:String) {
        for item in mang {
            let sodanh:So = So()
            sodanh.so = item
            sodanh.tien = tien
            sodanh.loai = .de
            mangDe.append(sodanh)
        }
        // Lọc đối tượng theo điều kiện
//        let mangde = mangquanly.filter { (so) -> Bool in
//            return so.so == "89"
////            return so.loai == .de
//        }
        var chuoi = ""
//        var tongtien = 0
        for item in mangDe {
//            let tiencuaso = item.tien
//            tongtien += Int(tiencuaso!)!
            let str = item.hienthi() // 89:20000
            chuoi.append(contentsOf: str)
        }
        deLabel.text = chuoi
        
    }
    func hienThiLo(mang:[String],tien:String) {
        
        for item in mang {
            let sodanh:So = So()
            sodanh.so = item
            sodanh.tien = tien
            sodanh.loai = .lo
            mangLo.append(sodanh)
        }
        var chuoi = ""
        for item in mangLo {
            let str = item.hienthi()
            chuoi.append(str)
        }
        loLabel.text = chuoi
        
    }
    
    func hienThiXien(mang:[String],tien:String) {
        var str = ""
        var kq = ""
        var chuoi = ""
        for item in mang {
            kq = item + "-"
            str.append(contentsOf: kq)
        }
        str.removeLast()
        let sodanh: So = So()
        sodanh.so = str
        sodanh.tien = tien
        sodanh.loai = .xien
        mangXien.append(sodanh)
        for item in mangXien {
            let STR = item.hienthi()
            chuoi.append(STR)
        }
        xienLabel.text = chuoi
        
    }
    func hienThiBaCang(mang:[String],tien:String) {
        var chuoi = ""
        for item in mang {
            let sodanh: So = So()
            sodanh.so = item
            sodanh.tien = tien
            sodanh.loai = .bacang
            mangBaCang.append(sodanh)
        }
        for item in mangBaCang {
            let str = item.hienthi()
            chuoi.append(str)
        }
        baCangLabel.text = chuoi
    }
    
    @IBAction func didSelectedLo(_ sender: Any) {
        danhdau = .lo
        customView.kieudanh = danhdau
        // hiện bàn phím
        hidenTextField.becomeFirstResponder()

    }
    
    @IBAction func didSelectedDe(_ sender: Any) {
        danhdau = .de
        customView.kieudanh = danhdau
        hidenTextField.becomeFirstResponder()
    }

    @IBAction func didSelectedBaCang(_ sender: Any) {
        danhdau = .bacang
        customView.kieudanh = danhdau
        hidenTextField.becomeFirstResponder()
    }
    
    @IBAction func didSelectedXien(_ sender: Any) {
        danhdau = .xien
        customView.kieudanh = danhdau
        hidenTextField.becomeFirstResponder()
    }
    
    @objc func textFieldDidChange () {
        // gán giá trị của textfield vào label tương ứng
        deLabel.text = hidenTextField.text
        loLabel.text = hidenTextField.text
        xienLabel.text = hidenTextField.text
        baCangLabel.text = hidenTextField.text
    }
    
    func hamanhinh() {
        // hạ bàn phím
        hidenTextField.resignFirstResponder()
        
    }
    
    func chuyenKQ(so: [String],tien:String) {
        if danhdau == .de {
            hienThiDe(mang: so,tien: tien)
            
        }else if danhdau == .lo {
            hienThiLo(mang: so,tien:tien)
            
        }else if danhdau == .bacang {
            hienThiBaCang(mang: so, tien: tien)
            
        }else if danhdau == .xien {
            hienThiXien(mang: so, tien: tien)
        }
    }
    
    @IBAction func btnLuuKq(_ sender: Any) {
        guard let delegate = self.delegate else {
            return
        }
        delegate.chuyenkq(de: mangDe, lo: mangLo, xien: mangXien, bacang: mangBaCang)
    }
}
