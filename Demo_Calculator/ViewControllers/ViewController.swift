//
//  ViewController.swift
//  Demo_Calculator
//
//  Created by Thang on 8/27/20.
//  Copyright © 2020 Thang. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var btnDangKy: UIButton!
    @IBOutlet weak var btnDangNhap: UIButton!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setUpElements()
        
    }
    
    func setUpElements () {

        ChinhSua.suamaubtn(btnDangKy)
        ChinhSua.suabtn(btnDangNhap)
    }

}

