//
//  BangSoDanhViewController.swift
//  Demo_Calculator
//
//  Created by Thang on 9/16/20.
//  Copyright © 2020 Thang. All rights reserved.
//

import UIKit
import Foundation
class BangSoDanhViewController: UIViewController, DanhDelegate {
    
//tạo 1 class có 2 thuộc tính là 1 chuỗi và 1 loại hình để hiển thị tên
//mỗi 1 chuỗi thì lại tương ứng với 1 loại Đề - Lô - Xiên - Ba càng
//mỗi 1 chuỗi thì lại tương ứng với các item của mảng + "\n"
    @IBOutlet weak var lblDe: UILabel!
    @IBOutlet weak var lblLo: UILabel!
    @IBOutlet weak var lblXien: UILabel!
    @IBOutlet weak var lblBaCang: UILabel!
    @IBOutlet weak var lblSoDe: UILabel!
    @IBOutlet weak var lblSoLo: UILabel!
    @IBOutlet weak var lblSoXien: UILabel!
    @IBOutlet weak var lblSoBaCang: UILabel!
    
    var mangde:[So] = []
    var manglo:[So] = []
    var mangxien:[So] = []
    var mangbacang:[So] = []
    var delegate: DanhDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        lblSoDe.numberOfLines = 0
        lblSoDe.lineBreakMode = .byWordWrapping
        lblSoLo.numberOfLines = 0
        lblSoLo.lineBreakMode = .byWordWrapping
        lblSoXien.numberOfLines = 0
        lblSoXien.lineBreakMode = .byWordWrapping
        lblSoBaCang.numberOfLines = 0
        lblSoBaCang.lineBreakMode = .byWordWrapping
        // Do any additional setup after loading the view.
        self.hienThi()
    }
    func hienThi () {
        var chuoi = ""
        var chuoi1 = ""
        var chuoi2 = ""
        var chuoi3 = ""
        for item in mangde {
            let str = item.hienthi()
            chuoi.append(str)
        }
        lblSoDe.text = chuoi
        for item in manglo {
            let str = item.hienthi()
            chuoi1.append(str)
        }
        lblSoLo.text = chuoi1
        for item in mangxien {
            let str = item.hienthi()
            chuoi2.append(str)
        }
        lblSoXien.text = chuoi2
        for item in mangbacang {
            let str = item.hienthi()
            chuoi3.append(str)
        }
        lblSoBaCang.text = chuoi3
    }
    func chuyenkq(de: [So], lo: [So], xien: [So], bacang: [So]) {
        mangde = de
        manglo = lo
        mangxien = xien
        mangbacang = bacang
    }


}
