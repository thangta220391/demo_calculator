//
//  Custom.swift
//  Demo_Calculator
//
//  Created by Thang on 8/27/20.
//  Copyright © 2020 Thang. All rights reserved.
//

import Foundation
import UIKit

class Custom {
    func customTxt(_ Textfield: UITextField) {
        let buttonLine = CALayer()
        buttonLine.frame = CGRect(x: 0, y: Textfield.frame.height - 2, width: Textfield.frame.width, height: 2)
        buttonLine.backgroundColor = UIColor.init(red: 48/255, green: 173/255, blue: 99/255, alpha: 1).cgColor
        Textfield.borderStyle = .none
        Textfield.layer.addSublayer(buttonLine)
    }
    
    func customBtn (_ button:UIButton) {
        button.layer.borderWidth = 2
        button.layer.borderColor = UIColor.black.cgColor
        button.layer.cornerRadius = 25.0
        button.tintColor = UIColor.black
    }
    func caiDatMK(_ passWord: String) -> Bool {
        let passwordTest = NSPredicate(format: "SELF MATCHES %@", "^(?=.*[a-z])(?=.*[$@$#!%*?&])[A-Za-z\\d$@$#!%*?&]{8,}")
        return passwordTest.evaluate(with: passWord)
    }
}
