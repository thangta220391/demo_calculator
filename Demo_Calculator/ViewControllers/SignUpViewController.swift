//
//  SignUpViewController.swift
//  Demo_Calculator
//
//  Created by Thang on 8/27/20.
//  Copyright © 2020 Thang. All rights reserved.
//

import UIKit
import FirebaseAuth
import FirebaseFirestore

class SignUpViewController: UIViewController {

    @IBOutlet weak var txtHoDem: UITextField!
    @IBOutlet weak var txtTen: UITextField!
    @IBOutlet weak var txtEmail: UITextField!
    @IBOutlet weak var txtMatKhau: UITextField!
    @IBOutlet weak var btnDangKy: UIButton!
    @IBOutlet weak var lblThongBao: UILabel!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setUpElements()
   
    }
    
    func setUpElements () {
        lblThongBao.alpha = 0
        
        ChinhSua.suaText(txtHoDem)
        ChinhSua.suaText(txtTen)
        ChinhSua.suaText(txtEmail)
        ChinhSua.suaText(txtMatKhau)
        ChinhSua.suamaubtn(btnDangKy)

    }
    func ktraText() -> String? {
        if txtHoDem.text?.trimmingCharacters(in: .whitespacesAndNewlines) == "" ||
            txtTen.text?.trimmingCharacters(in: .whitespacesAndNewlines) == "" ||
            txtEmail.text?.trimmingCharacters(in: .whitespacesAndNewlines) == "" ||
            txtMatKhau.text?.trimmingCharacters(in: .whitespacesAndNewlines) == "" {
            
            return "Điền đầy đủ thông tin"
        }
        
        let ktPass = txtMatKhau.text!.trimmingCharacters(in: .whitespacesAndNewlines)
        if ChinhSua.suaMK(ktPass) == false {
            return "Mật khẩu phải đủ 8 ký tự có chứa 1 ký tự đặc biệt và 1 số"
        }
        return nil
    }
    
    func  baoLoi(_ message: String) {
        lblThongBao.text = message
        lblThongBao.alpha = 1
    }
    
    @IBAction func btnAnDangKy(_ sender: Any) {
        
        let error = ktraText()
        if error != nil {
            baoLoi(error!)
        }
        else {
            let hoTen = txtHoDem.text!.trimmingCharacters(in: .whitespacesAndNewlines)
            let ten = txtTen.text!.trimmingCharacters(in:.whitespacesAndNewlines)
            let email = txtEmail.text!.trimmingCharacters(in: .whitespacesAndNewlines)
            let matkhau = txtMatKhau.text!.trimmingCharacters(in: .whitespacesAndNewlines)
        
            Auth.auth().createUser(withEmail: email, password: matkhau) { (result, err) in
                
                if err != nil {
                    self.baoLoi("Lỗi tạo tài khoản")
                }
                else {
                    let db = Firestore.firestore()
                    
                    db.collection("users").addDocument(data: ["firstname": hoTen, "lastname": ten, "uid": result?.user.uid]) { (error) in
                        
                        if error != nil {
                            self.baoLoi("Lỗi dữ liệu người dùng")
                        }
                    }
                    self.transitionToHom()
                }
            }
        
        
        }
           
    }
    
    
    func transitionToHom() {
    
        let homeViewController = storyboard?.instantiateViewController(identifier: ChuyenMH.Storyboard.homeViewController) as! HomViewController
        
        view.window?.rootViewController = homeViewController
        view.window?.makeKeyAndVisible()
        
    }
    

}
