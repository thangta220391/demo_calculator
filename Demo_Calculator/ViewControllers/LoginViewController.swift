//
//  LoginViewController.swift
//  Demo_Calculator
//
//  Created by Thang on 8/27/20.
//  Copyright © 2020 Thang. All rights reserved.
//

import UIKit
import FirebaseAuth

class LoginViewController: UIViewController {
    
    @IBOutlet weak var txtEmail: UITextField!
    @IBOutlet weak var txtMatKhau: UITextField!
    @IBOutlet weak var btnDangNhap: UIButton!
    @IBOutlet weak var lblThongBao: UILabel!
    @IBOutlet weak var loadingIndicator: UIActivityIndicatorView!

    
    override func viewDidLoad() {
        super.viewDidLoad()
        loadingIndicator.isHidden = true
        setUpElements()
  
    }
    
    func setUpElements() {
        
        lblThongBao.alpha = 0
        
        ChinhSua.suaText(txtEmail)
        ChinhSua.suaText(txtMatKhau)
        ChinhSua.suamaubtn(btnDangNhap)
        
    }
    
    @IBAction func DangNhap(_ sender: Any) {
        
        let email = txtEmail.text!.trimmingCharacters(in: .whitespacesAndNewlines)
        let pass = txtMatKhau.text!.trimmingCharacters(in: .whitespacesAndNewlines)
        if email == "admin" && pass == "1234" {
            let homeViewController = self.storyboard?.instantiateViewController(identifier: ChuyenMH.Storyboard.homeViewController) as! HomViewController
            
            self.navigationController?.pushViewController(homeViewController, animated: true)
        }else {
            loadingIndicator.isHidden = false
            loadingIndicator.startAnimating()
            Auth.auth().signIn(withEmail: email, password: pass) { (result, error) in
                DispatchQueue.main.async {
                    self.loadingIndicator.stopAnimating()
                    self.loadingIndicator.isHidden = true

                    if error != nil {
                        self.lblThongBao.text = error!.localizedDescription
                        self.lblThongBao.alpha = 1
                    }
                    else {
                        let homeViewController = self.storyboard?.instantiateViewController(identifier: ChuyenMH.Storyboard.homeViewController) as! HomViewController
                        self.navigationController?.pushViewController(homeViewController, animated: true)
                    }
                    
                }
            }
        }
    }
    

}
