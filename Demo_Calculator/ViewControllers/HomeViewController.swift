//
//  HomViewController.swift
//  Demo_Calculator
//
//  Created by Thang on 8/27/20.
//  Copyright © 2020 Thang. All rights reserved.
//

import UIKit

enum Pheptoan: String {
    case cong = "+"
    case tru  = "-"
    case nhan = "*"
    case chia = "/"
    case NULL = "Null"
}


class HomViewController: UIViewController {

    @IBOutlet weak var lbl: UILabel!
    
    var runningNumber = ""
    var leftvalua = ""
    var rightValua = ""
    var result = ""
    var thuHienPhepToan: Pheptoan = .NULL
    
    
        override func viewDidLoad() {
        super.viewDidLoad()
            
            lbl.text = "0"

    }

    @IBAction func btnNumber(_ sender: SuaNut) {
        if runningNumber.count <= 9 {
            runningNumber += "\(sender.tag)"
            lbl.text = runningNumber
        }
    }
    
    @IBAction func btnXoa(_ sender: SuaNut) {
        runningNumber = ""
        leftvalua = ""
        rightValua = ""
        result = ""
        thuHienPhepToan = .NULL
        lbl.text = "0"
    }
    
    @IBAction func btnPhay(_ sender: SuaNut) {
        if runningNumber.count <= 8 {
            runningNumber += "."
            lbl.text = runningNumber
        }
    }
    
    @IBAction func btnBang(_ sender: SuaNut) {
        pheptoan(pheptoan: thuHienPhepToan)
    }
    
    @IBAction func btnCong(_ sender: SuaNut) {
        pheptoan(pheptoan: .cong)
    }
    
    @IBAction func btnTru(_ sender: SuaNut) {
        pheptoan(pheptoan: .tru)
    }
    
    @IBAction func btnNhan(_ sender: SuaNut) {
        pheptoan(pheptoan: .nhan)
    }
    
    @IBAction func btnChia(_ sender: SuaNut) {
        pheptoan(pheptoan: .chia)
    }
    
    func pheptoan(pheptoan: Pheptoan) {
        if thuHienPhepToan != .NULL {
            if runningNumber != ""{
                rightValua = runningNumber
                runningNumber = ""
                
                if thuHienPhepToan == .cong {
                    result = "\(Double(leftvalua)! + Double(rightValua)!)"
                }else if thuHienPhepToan == .tru {
                result = "\(Double(leftvalua)! - Double(rightValua)!)"
                }else if thuHienPhepToan == .nhan {
                result = "\(Double(leftvalua)! * Double(rightValua)!)"
                }else if thuHienPhepToan == .chia {
                result = "\(Double(leftvalua)! / Double(rightValua)!)"
                }
                leftvalua = result
                if (Double(result)!.truncatingRemainder(dividingBy: 1) == 0) {
                    result = "\(Int(Double(result)!))"
                }
                lbl.text = result
            }
            thuHienPhepToan = pheptoan
        }else {
            leftvalua = runningNumber
            runningNumber = ""
            thuHienPhepToan = pheptoan
        }
    }
}
